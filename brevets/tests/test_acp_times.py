"""
Author: Abduarraheem Elfandi
Test cases for acp_times.py
"""

import acp_times
import nose
import logging
import arrow  # Replacement for datetime, based on moment.js

logging.basicConfig(format='%(levelname)s:%(message)s',
                    level=logging.WARNING)
log = logging.getLogger(__name__)

# Open time test cases
def test_zero_open_time():
	"""
	Openning time for control point at 0 is 0.
	"""
	control_dist_km = 0
	brevet_dist_km = 200
	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 0, minutes= 0).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

def test_open_time_basic():
	"""
	Follows basic opening time rules for different control points with different brevets.
	"""
	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 150
	brevet_dist_km = 200
	max_speed = 34	# max speed for the range 0-200
	time = control_dist_km/max_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 300
	brevet_dist_km = 400
	max_speed_1 = 32	# max speed for the range 200-400
	time = 200/max_speed + (control_dist_km-200)/max_speed_1
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 550
	brevet_dist_km = 600
	max_speed_2 = 30	# max speed for the range 400-600
	time = 200/max_speed + 200/max_speed_1 + (control_dist_km-400)/max_speed_2
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol


	control_dist_km = 890
	brevet_dist_km = 1000
	max_speed_3 = 28	# max speed for the range 600-1000
	time = 200/max_speed + 200/max_speed_1 + 200/max_speed_2 + (control_dist_km-600)/max_speed_3
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol


def test_open_time_longer_route():
	"""
	Use a distance of the brevet in the calculation, even though the route was slightly longer than the brevet.
	"""
	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 205
	brevet_dist_km = 200
	max_speed = 34	# max speed for the range 0-200
	time = brevet_dist_km/max_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	# assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 409
	brevet_dist_km = 400
	max_speed_1 = 32	# max speed for the range 200-400
	time = 200/max_speed + 200/max_speed_1
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 610
	brevet_dist_km = 600
	max_speed_2 = 30	# max speed for the range 400-600
	time = 200/max_speed + 200/max_speed_1 + 200/max_speed_2
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 1001
	brevet_dist_km = 1000
	max_speed_3 = 28	# max speed for the range 600-1000
	time = 200/max_speed + 200/max_speed_1 + 200/max_speed_2 + 400/max_speed_3
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.open_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol



# Close time test cases
def test_zero_close_time():
	"""
	Closing time for control point at 0 is 1H.
	"""

	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 0
	brevet_dist_km = 200
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 1, minutes= 0).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

def test_close_time_basic():
	"""
	Follows basic closing time rules for different control points with different brevets.
	"""

	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 100
	brevet_dist_km = 200
	min_speed = 15	# min speed for the range 0-200
	time = control_dist_km/min_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 300
	brevet_dist_km = 400
	min_speed_1 = 15	# min speed for the range 200-400
	time = control_dist_km/min_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 550
	brevet_dist_km = 600
	min_speed_2 = 15	# min speed for the range 400-600
	time = control_dist_km/min_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 890
	brevet_dist_km = 1000
	min_speed_3 = 11.428	# min speed for the range 600-1000
	time = 200/min_speed + 200/min_speed_1 + 200/min_speed_2 + (control_dist_km-600)/min_speed_3
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol



def test_close_time_rules():
	"""
	Follows the closing rules for each brevet, 13:30 for 200 KM, 20:00 for 300 KM, 27:00 for 400 KM, 40:00 for 600 KM, and 75:00 for 1000 KM.
	"""
	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 200
	brevet_dist_km = 200
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 13, minutes= 30).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol
	
	control_dist_km = 300
	brevet_dist_km = 300
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 20, minutes= 00).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 400
	brevet_dist_km = 400
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 27, minutes= 00).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 600
	brevet_dist_km = 600
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 40, minutes= 00).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 1000
	brevet_dist_km = 1000
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 75, minutes= 00).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol


def test_close_time_fail():
	"""
	The overall time limit for a 200km brevet is 13H30 and not 13H20, and overall time limit for a 400km is 27H and not 26H40
	"""
	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 200
	brevet_dist_km = 200
	min_speed = 15	# min speed for the range 0-200
	time = control_dist_km/min_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) != sol

	control_dist_km = 400
	brevet_dist_km = 400
	min_speed = 15	# min speed for the range 0-200
	time = control_dist_km/min_speed
	hours = int(time)
	mins = round((time - hours) * 60)
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= hours, minutes= mins).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) != sol

def test_close_time_km_irrelevant():
	"""
	The fact that the route is somewhat longer than the brevet is irrelevant for closing time.
	Based of Example 1 in:
	https://rusa.org/pages/acp-brevet-control-times-calculator
	"""

	brevet_start_time = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm')
	control_dist_km = 205
	brevet_dist_km = 200
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 13, minutes= 30).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 306
	brevet_dist_km = 300
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 20, minutes= 0).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 410
	brevet_dist_km = 400
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 27, minutes= 0).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 601
	brevet_dist_km = 600
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 40, minutes= 0).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol

	control_dist_km = 1001
	brevet_dist_km = 1000
	sol = arrow.get('2021-11-08 12:00', 'YYYY-MM-DD HH:mm').shift(hours= 75, minutes= 0).isoformat()
	assert acp_times.close_time(control_dist_km, brevet_dist_km, brevet_start_time) == sol